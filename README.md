Code that shows how to spawn a chrome browser instance using
Selenium and how to set a proxy for each instance.

Read more on our article at 
[scrapebot.com](https://scrapebot.com/how-to-set-a-proxy-in-selenium-and-chrome-browser/)