import os
import uuid
import zipfile

from selenium import webdriver
from selenium.webdriver.chrome.options import Options

def create_browser_instance(chrome_options):
	return webdriver.Chrome( os.path.join(
		#	make sure to download chromedriver binary and 
		#	place it next to this script
		#	binaries can be downloaded from:
		#	https://chromedriver.storage.googleapis.com/index.html
		os.path.dirname( os.path.abspath( __file__ ) ), 'chromedriver'
	), chrome_options = chrome_options )

def set_proxy(chrome_options, host, port, username, password):
	#	read the required files to make this proxy plugin
	manifest_json = open(os.path.join(
		os.path.dirname( os.path.abspath( __file__ ) ), 'proxy_plugin', 'manifest.json'
	) ).read()
	background_js = open( os.path.join(
		os.path.dirname( os.path.abspath( __file__ ) ), 'proxy_plugin', 'background'
	) ).read().format( username=username, password=password, host=host, port=port )

	#	build the plugin path
	pluginpath = '/tmp/{}.zip'.format( str(uuid.uuid4()) )

	#	zip everything into an archive
	with zipfile.ZipFile( pluginpath, 'w' ) as zp:
		zp.writestr( "manifest.json", manifest_json )
		zp.writestr( "background.js", background_js )
	
	#	finally, inject the extension
	chrome_options.add_extension( pluginpath )

def goto(url, browser):
	return browser.get(url)

if __name__ == "__main__":
	co = Options( )
	co.add_argument( "--no-sandbox" )

	#	SET YOUR PROXY
	proxy_host = os.environ.get("PROXY_HOST", "default_hostname")
	proxy_port = os.environ.get("PROXY_PORT", 34223)
	proxy_user = os.environ.get("PROXY_USER", "proxy_username")
	proxy_pass = os.environ.get("PROXY_PASS", "proxy_password")

	set_proxy(co, proxy_host, proxy_port, proxy_user, proxy_pass)

	browser = create_browser_instance(co)
	goto("https://infoip.io", browser)